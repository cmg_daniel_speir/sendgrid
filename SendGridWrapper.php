<?php
require_once __DIR__ . '/lib/loader.php';

use SendGrid\Content;
use SendGrid\Email;
use SendGrid\Mail;
use SendGrid\Response;

class SendGridWrapper {

  /**
   * @var string
   */
  private $sAPIKey = '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';

  /**
   * @var Mail
   */
  private $oMail;

  /**
   * @var SendGrid
   */
  protected $oSG;

  /**
   * @var Email
   */
  protected $oFrom;

  /**
   * @var Email
   */
  protected $oTo;

  /**
   * @var string
   */
  protected $sSubject;

  /**
   * @var Content
   */
  protected $oContent;

  /**
   * @var Response
   */
  protected $oResponse;

  /**
   * @var string
   */
  protected $sContent;

  /**
   * SendGridWrapper constructor.
   * @param null $sKey
   */
  public function __construct($sKey = null) {
    $this->oSG = new SendGrid($sKey ? $sKey : $this->sAPIKey);
  }

  /**
   * Constructs a SendGrid Email object.
   *
   * @param $sName
   * @param $sEmail
   * @return $this
   */
  public function from($sName, $sEmail) {
    $this->oFrom = new Email($sName, $sEmail);

    return $this;
  }

  /**
   * Construct a SendGrid Email object.
   * @param $sName
   * @param $sEmail
   * @return $this
   */
  public function to($sName, $sEmail) {
    $this->oTo = new Email($sName, $sEmail);

    return $this;
  }

  /**
   * Set the email subject.
   *
   * @param $sSubject
   * @return $this
   */
  public function subject($sSubject) {
    $this->sSubject = $sSubject;

    return $this;
  }

  /**
   * Constructs a SendGrid Content object.
   *
   * @param $sContent
   * @param bool $bHTML
   * @return $this
   */
  public function content($sContent, $bHTML = false) {
    $this->oContent = new Content($bHTML ? 'text/html' : 'text/plain', $sContent);
    $this->sContent = $sContent;

    return $this;
  }

  /**
   * Attempts to send the email.
   *
   * @return bool
   */
  public function attempt() {
    $this->oMail = new Mail($this->oFrom, $this->sSubject, $this->oTo, $this->oContent);
    $this->oResponse = $this->oSG->client->mail()->send()->post($this->oMail);

    return $this->oResponse->statusCode() == 202;
  }

  /**
   * Returns the provided email content.
   *
   * @return mixed
   */
  public function getContent() {
    return $this->sContent;
  }

  /**
   * Returns a SendGrid Response object.
   *
   * @return Response
   */
  public function getResponse() {
    return $this->oResponse;
  }

  /**
   * Returns the Response status code.
   *
   * @return int|null
   */
  public function getResponseStatusCode() {
    return $this->oResponse->statusCode();
  }

  /**
   * Returns the Response body.
   *
   * @return mixed
   */
  public function getResponseBody() {
    return $this->oResponse->body();
  }

  /**
   * Parses and returns the Response body, if one exists.
   *
   * @return mixed|null
   */
  public function getParsedResponseBody() {
    if ($sBody = $this->getResponseBody()) {
      return json_decode($sBody);
    }

    return null;
  }

  /**
   * Returns SendGrid's message ID value as returned via the
   * response headers.
   *
   * @return null|string
   */
  public function getMessageId() {
    if ($oResponse = $this->getResponse()) {
      $aHeaders = $oResponse->headers();

      if ($aMessageId = preg_grep('/^x-message-id/i', $aHeaders ? $aHeaders : [])) {
        $sHeader = reset($aMessageId);

        return trim(preg_replace('/^x-message-id\:\s+/i', '', $sHeader));
      }
    }

    return null;
  }
}