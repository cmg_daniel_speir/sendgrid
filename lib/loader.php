<?php
/**
 * Allows us to include one file instead of two when working without composer.
 */
require_once __DIR__ . '/helpers/mail/Client.php';
require_once __DIR__ . '/helpers/mail/Response.php';
require_once __DIR__ . '/SendGrid.php';
require_once __DIR__ . '/helpers/mail/Mail.php';
